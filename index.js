/* 1. Tìm số nguyên dương nhỏ nhất sao cho:
1 + 2 + … + n > 10000 */
var tong = 1;
n = 0;
while (tong < 10000) {
    n += 1;
    tong += n;
}

document.getElementById('ketQua1').innerHTML = n;

/* 2. Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2

+ x^3 + … + x^n (Sử dụng vòng lặp và hàm)*/
document.getElementById('Tinh2').onclick = function () {
    var sox = Number(document.getElementById('sox').value);
    var son = Number(document.getElementById('son').value);
    Sn = 0;
    for (n = 1; n <= son; n++) {
        Sn += sox ** n;
    }
    document.getElementById('ketQua2').innerHTML = Sn;
}


/*3. Nhập vào n. Tính giai thừa 1*2*...n */

document.getElementById('Tinh3').onclick = function () {
    var soN = Number(document.getElementById('soN').value);
    Giaithua = 1;
    for (n = 1; n <= soN; n++) {
        Giaithua = Giaithua * n;
    }
    document.getElementById('ketQua3').innerHTML = Giaithua;
}

/*4. Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div.
Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì
background màu xanh.*/

document.getElementById('Tinh4').onclick = function () {
    var div2 = '<div style="background-color: red;" class="text-white">thẻ lẻ</div>'
    var div1 = '<div style="background-color: blue;" class="text-white">thẻ chẵn</div>'
    output = '';
    for(n = 1; n <=10; n++){
        if(n % 2 ==0){
            output += div2
        } else{
            output += div1
        }
    }

    document.getElementById('ketQua4').innerHTML = output;
}
